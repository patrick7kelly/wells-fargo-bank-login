from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import time
import re
import os
import datetime
import pdfquery
import requests
import itertools
from pdfminer.pdfparser import PDFSyntaxError

def wells_fargo_login(username, password):
    # create initial dicts & lists
    output = {'credentials': {'username': username, 'password': 'hidepassword'}, 'bank_info': {'bank_name': 'wells', 'bank_long_name': 'Wells Fargo'}, 'mfa': [], 'user_info': {}, 'accounts': [], 'cookies': '', 'updated_on': datetime.datetime.now()}
    cash_accounts_urls = []

    # initiate webdriver & attempt to login
    browser = webdriver.Firefox()
    browser.get("https://connect.secure.wellsfargo.com/auth/login/present")
    browser.execute_script("document.getElementById('j_username').setAttribute('value', '{0}')".format(username))
    browser.execute_script("document.getElementById('j_password').setAttribute('value', '{0}')".format(password))
    browser.find_element_by_name("continue").click()
    # TODO: catch any promo that is displayed (click 'continue' on page to keep going forward)
    # retry while we wait for the login process to conclude
    while True:
        try:
            if browser.title == 'Wells Fargo - Please confirm your identity':
                # switch_to_mfa
                browser.find_element_by_id('indict_getCode').click()
                browser.execute_script("document.getElementById('indict_code').setAttribute('value', '{0}')".format('verification_code_goes_here'))
                browser.find_element_by_id('indict_submit').click()
                print 'mfa'
            elif 'error=yes' in browser.current_url:
                print 'incorrect username/password'
                print 'BL05'
            elif browser.find_element_by_xpath("//table[@id='cash']//tbody"):
                cash_accounts = browser.find_element_by_xpath("//table[@id='cash']//tbody")
            elif browser.find_element_by_id('online_wellsfargo_com_splash'):
                # attempt to bypass WF promo page (appears before account summary page)
                try:
                    print browser.find_element_by_name('Continue')
                    print 'probs promo'
                    browser.find_element_by_name('Continue').click()
                except NoSuchElementException:
                    pass
            else:
                print 'whole login failed, throw error'
        except NoSuchElementException:
            time.sleep(1)
        else:
            # TODO: fix cash_accounts potentially referenced before declared
            for account in cash_accounts.find_elements(By.TAG_NAME, "tr"):
                url = account.find_elements(By.TAG_NAME, "a")[0]
                url = url.get_attribute('href')
                cash_accounts_urls.append(url)
            online_statements_link = browser.find_elements_by_link_text('View online statements')[0].get_attribute('href')
            user_info_link = browser.find_element_by_xpath("//div[@id='asrEmail']//a").get_attribute('href')
            break

    for account in cash_accounts_urls:
        account_output = {'type': 'depository', 'balance': {}, 'meta': {}, 'numbers': {}, 'transactions': []}

        # navigate to account page
        browser.get(account)
        browser.find_element_by_xpath("//a[@id='accountAndRoutingNumbers']").click()
        account_output['balance'] = {'available': re.sub(r'((?<=\d),(?=\d))|(\$(?=\d))', r'', browser.find_element_by_css_selector(".availableBalanceTotalAmount").text)}
        account_name = Select(browser.find_element_by_id("accountDropdown"))
        account_name = re.sub(r'X+', '- ', account_name.first_selected_option.text)
        account_output['meta'] = {'name': account_name}

        # account/routing numbers do not appear initially, retry until they are visible
        while True:
            try:
                account_num = browser.find_element_by_xpath("//div[@id='accountAndRoutingNumbersContent']//div[@class='unmask-account-number-popover']//div//span").text
                account_routing = browser.find_element_by_xpath("//div[@id='accountAndRoutingNumbersContent']//div[@class='unmask-account-number-popover']//div[2]//span[2]").text
                if account_num:
                    account_output['numbers'] = {'account': account_num, 'routing': account_routing}
                    break
                else:
                    # clicking again can cause the acct/routing numbers to appear faster
                    browser.find_element_by_xpath("//a[@id='accountAndRoutingNumbers']").click()
                    time.sleep(1)
            except NoSuchElementException:
                print 'return BNL06'

        # select WF preference for daily balances
        browser.find_element_by_xpath("//div[@id='preferences']//div[@id='justLink']//a").click()
        if not browser.find_element_by_id("endingBalanceSelector").is_selected():
            browser.find_element_by_id("endingBalanceSelector").click()
            browser.find_element_by_name("preferencesCommand.buttonName").click()

        # fetch + save recent tv history (defaults to past 90 days)
        transaction_table = browser.find_element_by_xpath("//table[@id='DDATransactionTable']//tbody")
        for transaction in transaction_table.find_elements(By.TAG_NAME, "tr"):
            transaction_output = {}

            # test whether tx has posted or not
            skip = False
            for column in transaction.find_elements_by_tag_name("th"):
                if 'sectionHeader' in column.get_attribute('class'):
                    skip = True
                transaction_output['date'] = column.text
                if 'postedHeader' in column.get_attribute('headers'):
                    transaction_output['pending'] = 0
                else:
                    transaction_output['pending'] = 1
            transaction_columns = transaction.find_elements(By.TAG_NAME, "td")
            for column in transaction_columns:
                if column.get_attribute('id') == 'noPendingMessage':
                    skip = True
                if 'descriptionHeader' in column.get_attribute('headers'):
                    transaction_output['description'] = column.text if column.text else ''
                elif column.text != ' ' and any(x in column.get_attribute('headers') for x in ['depositsConsumerHeader', 'withdrawalsConsumerHeader']):
                    transaction_output['amount'] = re.sub(r'((?<=\d),(?=\d))|(\$(?=\d))', r'', column.text)
                    if 'depositsConsumerHeader' in column.get_attribute('headers'):
                        transaction_output['debit'] = 0
                    elif 'withdrawalsConsumerHeader' in column.get_attribute('headers'):
                        transaction_output['debit'] = 1
                elif 'endingBalanceHeader' in column.get_attribute('headers'):
                    transaction_output['current_balance'] = re.sub(r'((?<=\d),(?=\d))|(\$(?=\d))', r'', column.text)
            if not skip:
                account_output['transactions'].append(transaction_output)
        output['accounts'].append(account_output)

    # fetch and scrape user_info
    browser.get(user_info_link)
    output['user_info'] = {'addresses': [], 'emails': [], 'names': []}
    while True:
        try:
            user_address_street1 = browser.find_element_by_name("residentialAddressLine1").get_attribute("value")
            user_address_street2 = browser.find_element_by_name("residentialAddressLine2").get_attribute("value")
            user_address_city = browser.find_element_by_name("residentialAddressCity").get_attribute("value")
            user_address_state = browser.find_element_by_name("residentialAddressState").get_attribute("value")
            user_zipcode5 = browser.find_element_by_name("residentialAddressPostalCode").get_attribute("value")
            user_zipcode4 = browser.find_element_by_name("residentialAddressZipCode4").get_attribute("value")
            user_address_street = '{0}, {1}'.format(user_address_street1, user_address_street2) if user_address_street2 else user_address_street1
            user_zipcode = '{0}-{1}'.format(user_zipcode5, user_zipcode4) if user_zipcode4 else user_zipcode5
            user_address = {'city': user_address_city,'state': user_address_state,'street': user_address_street,'zipcode': user_zipcode}
            output['user_info']['addresses'].append(user_address)
            break
        except NoSuchElementException:
            # user_info may not appear fast enough, wit and retry
            time.sleep(1)
    try:
        user_meail_primary = {'data': browser.find_element_by_name("primaryEmailAddress").get_attribute("value"),'type': 'primary'}
        output['user_info']['emails'].append(user_meail_primary)
    except NoSuchElementException:
        print 'does this mean not setup(?), or throw general error (BL09'
        pass
    try:
        user_meail_secondary = browser.find_element_by_name("secondaryEmailAddress").get_attribute("value")
        if user_meail_secondary.strip():
            output['user_info']['emails'].append({'data': user_meail_secondary,'type': 'secondary'})
    except NoSuchElementException:
        pass
    # retrieve a PDF statement to get user's full name
    browser.get(online_statements_link)

    while True:
        try:
            file_url = browser.find_element_by_xpath("//div[@class='content']//div[@class='documents']//div[@class='document-details']//a[contains(text(), 'Statement')]").get_attribute('data-url')
            file_name = file_url.split('/')[-1].split('?')[0]
            session = requests.Session()
            cookies = browser.get_cookies()
            for cookie in cookies:
                session.cookies.set(cookie['name'], cookie['value'])
            response = session.get('https://connect.secure.wellsfargo.com{0}'.format(file_url))
            file = open(file_name, "wb")
            file.write(response.content)
            file = open(file_name, "rb")
            try:
                pdf = pdfquery.PDFQuery(file)
                pdf.load()
                try:
                    user_fullname1 = [' '.join(items)for _, items in itertools.groupby(pdf.pq('LTTextLineHorizontal:in_bbox("%s, %s, %s, %s")' % (40.08, 604.638, 114.751, 614.926)).text().split(), str.isupper)][0]
                    output['user_info']['names'].append(user_fullname1)
                except IndexError:
                    # throw error, couldn't get user's fullname
                    pass
                try:
                    user_fullname2 = [' '.join(items)for _, items in itertools.groupby(pdf.pq('LTTextLineHorizontal:in_bbox("%s, %s, %s, %s")' % (40.08, 592.398, 126.143, 602.686)).text().split(), str.isupper)][0]
                    print user_fullname2
                    output['user_info']['names'].append(user_fullname2)
                except IndexError:
                    # no additional names
                    pass
                # delete PDF, it is no longer needed
                os.remove(file_name)
                break
            except PDFSyntaxError:
                time.sleep(1)
        except NoSuchElementException:
            time.sleep(1)
    browser.quit()
    return output